const content = document.querySelector("main");
//render funksiyasi
const renderList = () => {
  console.log("render");
  content.innerHTML = "";
  lists.map((list, index) => {
    content.innerHTML += `
        <div class = "list col-md-4">
        <div class="d-flex align-items-start justify-content-between">
            <p>${list.title}</p>
            <button class="btn" onclick = "deleteList(${index})" ><i class="fa-solid fa-x"></i></button>
            </div>
            <div class="d-flex gap-2">
                <input type="text" class="form-control" placeholder="Add New Task"/>
                <button class="btn btn-primary" onclick = "addNewTask(${index})">Add</button>
            </div>
            <ul class = "list-group mt-2" id="listUl">
            ${list.tasks
              .map((task, indexTask) => {
                return `
                <li class= "list-group-item d-flex justify-content-between align-items-center">
                    <input type="text"
                    class= "form-control border-0 shadow-none px-0"
                    value="${task.title}"/>
                    <button onclick="completed(${index}, ${indexTask})" class= "btn btn-warning btn-sm me-2">
                    <i class="fa-solid fa-check"></i>
                    </button>
                    <button class= "btn btn-danger btn-sm " onclick = "deleteTask(${index}, ${indexTask})">
                    <i class="fa-solid fa-trash"></i>
                    </button>
                </li>
                `;
              })
              .join("")}
            </ul>
        </div>
        `;
  });
  content.innerHTML += `
    <div class="list ">
        <div class="d-flex gap-2">
            <input type="text" class="form-control" placeholder="Add New List" id="list-name" />
            <button class="btn btn-primary" onclick="addNewList()">+</button>
        </div>
    </div>
  `;
};

const validation = (title) => title.trim().length > 0;
// local storage ga saqlash
const saveToLocalStorage = () => {
  localStorage.setItem("lists", JSON.stringify(lists));
};
const ul=document.getElementById("#listUl")
const completed = (taskIndex) => {
    console.log(taskIndex);
    for(let li in ul){
        if(li==taskIndex+1){
            li.style.add=""
        }
    }
}
// yengi list yaratish funksiyasi
const addNewList = () => {
  const listNameInput = document.querySelector("#list-name");

  if (!validation(listNameInput.value)) return;

  lists.push({ title: listNameInput.value, tasks: [] });
  renderList();
  saveToLocalStorage();
};
//yengi task yaratish funksiyasi
const addNewTask = (listIndex) => {
  const title = document.querySelector(
    `.list:nth-child(${listIndex + 1}) input`
  ).value;
  console.log(title);

  if (!validation(title)) return;

  lists[listIndex].tasks.push({ title });

  renderList();
  saveToLocalStorage();
};
// task ni o'chirish funksiyasi
const deleteTask = (listIndex, taskIndex) => {
  console.log(listIndex, taskIndex);

  const tasks = lists[listIndex].tasks;
  lists[listIndex].tasks = [
    ...tasks.slice(0, taskIndex),
    ...tasks.slice(taskIndex + 1),
  ];
  renderList();
  saveToLocalStorage();
};
//list ni o'chirish funksiaysi
const deleteList = (listIndex) => {
  console.log(listIndex);

  lists = lists.filter((list, index) => index != listIndex);

  renderList();
  saveToLocalStorage();
};
// onload da chizib beradigan funksiya
const init = () => {
  renderList();
};
